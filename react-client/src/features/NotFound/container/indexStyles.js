import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  notFoundWrapper: {
    textAlign: 'center',
    '& h1': {
      fontSize: '124px',
      fontWeight: 600,
      color: 'rgba(0, 0, 0, 0.84)',
      marginBottom: 0,
    },
    '& h2': {
      fontSize: '44px',
      color: 'rgba(0, 0, 0, 0.3)',
      fill: 'rgba(0, 0, 0, 0.3)',
    },
  },
}));
