import React from 'react';
import { Link } from 'react-router-dom';

import { useStyles } from './indexStyles';

const NotFound = () => {
  const classes = useStyles();

  return (
    <div className={classes.notFoundWrapper}>
      <h1>404</h1>
      <h2>Not Found Page</h2>
      <p>
        Go to <Link to="/">Main Page</Link>
      </p>
    </div>
  );
};

export default NotFound;
