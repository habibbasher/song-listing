import React from 'react';
import Card from '@material-ui/core/Card';
import CardContent from '@material-ui/core/CardContent';
import Typography from '@material-ui/core/Typography';

import { useStyles } from './indexStyles';

import image from '../../../assets/images/img1.png';

const CardItem = ({ songTitle, artist, publisher }) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <img src={image} style={{ width: '120px' }} alt="Album image" />
      <div className={classes.details}>
        <CardContent className={classes.content}>
          <Typography variant="subtitle2" color="textSecondary">
            Song: <strong>{songTitle}</strong>
          </Typography>
          <Typography variant="subtitle2" color="textSecondary" gutterBottom>
            Artist: <strong>{artist}</strong>
          </Typography>
          <Typography variant="subtitle2" color="textSecondary">
            PublishedBy: <strong>{publisher}</strong>
          </Typography>
        </CardContent>
      </div>
    </Card>
  );
};

export default CardItem;
