import React from 'react';
import { Grid, Typography } from '@material-ui/core';

const Banner = ({ heading }) => {
  return (
    <div>
      <Grid container direction="column" alignItems="center">
        <Grid item>
          <Typography variant="h3" gutterBottom>
            Song Listing
          </Typography>
        </Grid>
        <Grid item container direction="row" justify="center">
          <h4
            style={{
              width: '500px',
              textAlign: 'center',
              borderBottom: '1px solid #000',
              lineHeight: '0.1em',
              margin: '10px 0 20px',
            }}
          >
            <span style={{ background: '#fff', padding: '0 10px' }}>
              {heading}
            </span>
          </h4>
        </Grid>
      </Grid>
    </div>
  );
};

export default Banner;
