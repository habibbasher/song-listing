import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';

const AuthRoute = ({ component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props =>
        rest.isAuth ? <Component {...props} /> : <Redirect to="/" />
      }
    />
  );
};

// AuthRoute.propTypes = {
//   isAuth: PropTypes.bool.isRequired,
//   component: PropTypes.func.isRequired,
// };

export default AuthRoute;
