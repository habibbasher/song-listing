import React, { useState } from 'react';
import Pagination from '@material-ui/lab/Pagination';

import { useStyles } from './indexStyles';
import SelectField from '../SelectField';

const songsPerPage = [
  {
    value: 5,
  },
  {
    value: 10,
  },
  {
    value: 15,
  },
  {
    value: 20,
  },
];

const PaginationComponent = () => {
  const classes = useStyles();
  const [noOfSongs, setNoOfSongs] = useState(10);

  const changeHandler = (event, page) => {
    console.log('changeHandler -> page', page);
  };

  const getSongs = event => {
    setNoOfSongs(event.target.value);
  };

  return (
    <div
      style={{
        width: '100%',
        display: 'flex',
        justifyContent: 'space-between',
        padding: '0 30px',
      }}
    >
      <div className={classes.root}>
        <Pagination count={10} size="small" onChange={changeHandler} />
      </div>
      <div>
        <SelectField
          noOfSongs={noOfSongs}
          items={songsPerPage}
          label="Show:"
          handleChange={getSongs}
        />
      </div>
    </div>
  );
};

export default PaginationComponent;
