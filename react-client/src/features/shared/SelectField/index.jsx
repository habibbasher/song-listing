import React from 'react';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';

import { useStyles } from './indexStyles';

const SelectField = ({ noOfSongs, items, label, handleChange }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <p style={{ paddingTop: '15px' }}>{label}</p>
      <FormControl className={classes.formControl}>
        <Select
          labelId="simple-select-label"
          id="simple-select"
          value={noOfSongs}
          onChange={handleChange}
        >
          {items &&
            items.map(item => (
              <MenuItem key={item.value} value={item.value}>
                {item.value}
              </MenuItem>
            ))}
        </Select>
      </FormControl>
      <p style={{ paddingTop: '15px' }}>Songs/Page</p>
    </div>
  );
};

export default SelectField;
