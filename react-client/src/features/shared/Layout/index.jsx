import React from 'react';
// import PropTypes from 'prop-types';
// import { connect } from 'react-redux';

// import { logoutHandler } from '../../Auth/Signin/actions';
import Feature from './Feature';
import { Auth } from './Auth';

const Layout = props => {
  return props.isAuth ? (
    <Feature logout={props.logoutHandler}>{props.children}</Feature>
  ) : (
    <Auth>{props.children}</Auth>
  );
};

export default Layout;

// Layout.propTypes = {
//   isAuth: PropTypes.bool.isRequired,
//   logoutHandler: PropTypes.func.isRequired,
// };

// const mapStateToProps = (state) => ({
//   isAuth: state.auth.signin.isAuth,
// });

// export const Layout = connect(mapStateToProps, { logoutHandler })(
//   LayoutContainer
// );
