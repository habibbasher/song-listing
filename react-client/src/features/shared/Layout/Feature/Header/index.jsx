import React, { useState } from 'react';
import { Link } from 'react-router-dom';
import { Menu } from 'antd';

const { SubMenu, Item } = Menu;

const Header = () => {
  const [current, setCurrent] = useState('home');

  const handleClick = ev => {
    setCurrent(ev.key);
  };

  return (
    <Menu
      onClick={handleClick}
      selectedKeys={[current]}
      mode="horizontal"
      style={{ paddingLeft: '30px', paddingRight: '30px' }}
    >
      <Item key="home">
        <Link to="/">Home</Link>
      </Item>
      <Item key="register">
        <Link to="/mylist">My List</Link>
      </Item>
      <Item key="login">
        <Link to="/auth">Log Out</Link>
      </Item>

      <Item key="search" style={{ float: 'right' }}>
        <Link to="/auth">Log Out</Link>
      </Item>
    </Menu>
  );
};

export default Header;
