import React from 'react';
import PropTypes from 'prop-types';
import { Layout } from 'antd';

import Header from './Header';

const { Content } = Layout;

const Feature = ({ children, logout }) => {
  return (
    <Layout style={{ minHeight: '100vh' }}>
      <Header logout={logout} />

      <Layout style={{ padding: '0 50px' }}>
        <Content>{children}</Content>
      </Layout>
    </Layout>
  );
};

// Feature.propTypes = {
//   logout: PropTypes.func.isRequired,
// };

export default Feature;
