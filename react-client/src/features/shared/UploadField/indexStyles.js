import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  antUpload: {
    width: 128,
    height: 128,
  },
}));
