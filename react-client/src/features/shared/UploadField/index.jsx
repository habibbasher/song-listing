import React, { useState } from 'react';
import { connect } from 'react-redux';
import { Upload, message } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';

import { useStyles } from './indexStyles';
import { songImageProcess } from '../../Song/actions';

function getBase64(img, callback) {
  const reader = new FileReader();
  reader.addEventListener('load', () => callback(reader.result));
  reader.readAsDataURL(img);
}

function beforeUpload(file) {
  const isValidType = file.type === 'image/jpeg' || file.type === 'image/png';
  if (!isValidType) {
    message.error('You can only upload JPG/PNG file!');
  }

  const isLt2M = file.size / 1024 / 1024 < 2;
  if (!isLt2M) {
    message.error('Image must smaller than 2MB!');
  }
  return isValidType && isLt2M;
}

const INITIAL_UPLOAD_STATE = { isLoading: false, imageUrl: '' };

const UploadField = props => {
  const classes = useStyles();

  const [uploadState, setUploadState] = useState(INITIAL_UPLOAD_STATE);

  const handleChange = info => {
    if (info.file.status === 'uploading') {
      setUploadState({ ...uploadState, isLoading: true });
      return;
    }
    if (info.file.status === 'done') {
      props.songImageProcess({
        imageUrl: info.file.response.imageUrl,
        imageName: info.file.response.name,
      });
      getBase64(info.file.originFileObj, imageUrl => {
        setUploadState({
          ...uploadState,
          isLoading: false,
          imageUrl: imageUrl,
        });
      });
    }
  };

  const uploadButton = (
    <div>
      {uploadState.isLoading ? <LoadingOutlined /> : <PlusOutlined />}
      <div style={{ marginTop: 8 }}>Upload</div>
    </div>
  );

  return (
    <>
      <Upload
        name="file"
        listType="picture-card"
        showUploadList={false}
        action="http://localhost:3003/api/file/upload"
        beforeUpload={beforeUpload}
        onChange={handleChange}
      >
        {uploadState.imageUrl ? (
          <img
            src={uploadState.imageUrl}
            alt="avatar"
            style={{ width: '100%' }}
          />
        ) : (
          uploadButton
        )}
      </Upload>
    </>
  );
};

export default connect(null, { songImageProcess })(UploadField);
