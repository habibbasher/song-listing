import { api } from '../../helpers/api';

export const SONG_REQUEST_PROCESS = 'SONG_REQUEST_PROCESS';
export const SONG_REQUEST_ERROR = 'SONG_REQUEST_ERROR';
export const SONG_REQUEST_SUCCESS = 'SONG_REQUEST_SUCCESS';
export const SONG_IMAGE_PROCESS = 'SONG_IMAGE_PROCESS';

export const songImageProcess = payload => ({
  type: SONG_IMAGE_PROCESS,
  payload,
});

export const songRequestProcess = () => ({
  type: SONG_REQUEST_PROCESS,
});

export const songRequestSuccess = data => ({
  type: SONG_REQUEST_SUCCESS,
  data,
});

export const songRequestError = error => ({
  type: SONG_REQUEST_ERROR,
  error,
});

export const songFetchRequest = () => async dispatch => {
  try {
    dispatch(songRequestProcess());

    const data = await api('get', 'users');

    dispatch(songRequestSuccess(data));
  } catch (error) {
    dispatch(songRequestError(error.response ? error.response.data : error));
  }
};
