import Songs from './Songs';
import Create from './Create';
import Update from './Update';

export const routes = [
  {
    path: '/songs',
    component: Songs,
    isAuth: false,
    exact: true,
  },
  {
    path: '/song',
    component: Create,
    isAuth: false,
    exact: true,
  },
  {
    path: '/song/:id',
    component: Update,
    isAuth: false,
    exact: true,
  },
];
