import React from 'react';
import { Link } from 'react-router-dom';

import { Grid, Divider, Typography } from '@material-ui/core';
import Icon from '@material-ui/core/Icon';

import Banner from '../../shared/Banner';

import CardItem from '../../shared/Card';
import PaginationComponent from '../../shared/Pagination';
import { useStyles } from './indexStyles';

const songs = [
  {
    id: '1',
    title: 'Habib new',
    artist: 'Habib',
    publisher: 'Habib',
  },
  {
    id: '2',
    title: 'Habib new 1',
    artist: 'jack',
    publisher: 'Habib',
  },
  {
    id: '3',
    title: 'Habib new 2',
    artist: 'Balam',
    publisher: 'Habib',
  },
  {
    id: '4',
    title: 'Habib new 3',
    artist: 'Habib',
    publisher: 'Habib',
  },
];

const Songs = () => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      <Banner heading="My List" />
      <Grid container direction="row">
        <PaginationComponent />
      </Grid>
      <Divider style={{ margin: '0 30px' }} />
      <Grid container direction="row" style={{ margin: '0 30px' }}>
        {!songs.length ? (
          <Grid
            item
            container
            direction="column"
            alignItems="center"
            className={classes.empty}
          >
            <Grid item container direction="column" alignItems="center">
              <Typography variant="body2" gutterBottom>
                You haven't added any song yet!
              </Typography>
              <Typography variant="body2" style={{ marginTop: '20px' }}>
                Let's get started...
              </Typography>
            </Grid>
            <Link to="/song">
              <Grid item className={classes.iconContainer}>
                <Icon className={classes.icon}>add_circle</Icon>
              </Grid>
              <p style={{ marginTop: '10px' }}>Add New Song</p>
            </Link>
          </Grid>
        ) : (
          <>
            {songs.map(song => (
              <CardItem
                key={song.id}
                songTitle={song.title}
                artist={song.artist}
                publisher={song.publisher}
              />
            ))}
          </>
        )}
      </Grid>
    </div>
  );
};

export default Songs;
