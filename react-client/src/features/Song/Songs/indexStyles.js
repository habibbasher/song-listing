import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  root: {
    marginTop: '50px',
  },
  empty: {
    marginTop: '25%',
  },
  iconContainer: {
    border: '1px solid green',
    padding: '30px',
    borderRadius: '50%',
    marginTop: '30px',
  },
  icon: {
    fontSize: '30px',
    color: 'green',
  },
}));
