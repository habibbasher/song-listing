import {
  SONG_REQUEST_PROCESS,
  SONG_REQUEST_ERROR,
  SONG_REQUEST_SUCCESS,
  SONG_IMAGE_PROCESS,
} from './actions';

const initialState = {
  data: [],
  isLoading: false,
  isError: false,
  errorMessage: '',
  documents: [],
};

export const songReducer = (state = initialState, action) => {
  console.log('action ', action);
  switch (action.type) {
    case SONG_REQUEST_PROCESS:
      return { ...state, isError: false, errorMessage: '', isLoading: true };
    case SONG_REQUEST_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        errorMessage: action.error.message,
      };
    case SONG_REQUEST_SUCCESS:
      return {
        ...state,
        data: action.data,
        isLoading: false,
      };
    case SONG_IMAGE_PROCESS:
      return {
        ...state,
        documents: [...state.documents, action.payload],
        isLoading: false,
      };
    default:
      return state;
  }
};
