import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    marginTop: '25%',
    marginLeft: '50%',
    transform: 'translate(-50%, -25%)',
  },
  mb: {
    marginBottom: '4em',
  },
  error: {
    color: '#f44336',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  buttons: {
    display: 'flex',
    justifyContent: 'center',
  },
  button: {
    marginTop: theme.spacing(3),
    marginLeft: theme.spacing(1),
  },
  wrapper: {
    margin: theme.spacing(1),
    position: 'relative',
  },
  buttonProgress: {
    position: 'absolute',
    top: '50%',
    left: '50%',
  },
  upload: {
    textAlign: 'center',
    marginTop: '20px',
  },
}));
