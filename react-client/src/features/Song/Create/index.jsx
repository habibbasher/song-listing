import React from 'react';

import { Button, CircularProgress, Grid, Typography } from '@material-ui/core';
import { Card } from 'antd';
import { Formik, Form } from 'formik';

import initialValues from '../SongForm/initialValues';
import validationSchema from '../SongForm/validation';
import model from '../SongForm/model';

import { useStyles } from './indexStyles';
import SongForm from '../SongForm';
import UploadField from '../../shared/UploadField/index';

import { getCurrentUser } from '../../../helpers/auth';

const currentUser = getCurrentUser();

const { formId, formField } = model['createSongForm'];

const Create = () => {
  const classes = useStyles();

  const _handleSubmit = async (values, actions) => {
    console.log('values ', values);
    const song = {
      ...values,
      publishedBy: currentUser.user.id,
      coverImgLink: 'cover image link',
    };
    console.log('song ', song);
  };

  return (
    <div className={classes.root}>
      <Grid container direction="column" alignItems="center">
        <Grid item className={classes.mb}>
          <Typography variant="h3" gutterBottom>
            Song Listing
          </Typography>
        </Grid>
        <Grid item>
          <div className="card-container">
            <Card style={{ width: 400 }}>
              <Formik
                initialValues={initialValues['initialCreateSongFormValue']}
                validationSchema={validationSchema['createSongFormValidation']}
                onSubmit={_handleSubmit}
              >
                {({ isSubmitting }) => (
                  <Form id={formId}>
                    <SongForm formField={formField} />
                    <div className={classes.upload}>
                      <UploadField />
                    </div>

                    <div className={classes.buttons}>
                      <div className={classes.wrapper}>
                        <Button
                          disabled={isSubmitting}
                          type="submit"
                          variant="contained"
                          color="primary"
                          className={classes.button}
                        >
                          Publish
                        </Button>
                        {isSubmitting && (
                          <CircularProgress
                            size={24}
                            className={classes.buttonProgress}
                          />
                        )}
                      </div>
                    </div>
                  </Form>
                )}
              </Formik>
            </Card>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default Create;
