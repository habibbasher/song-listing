import * as Yup from 'yup';

import model from './model';

const { createSongForm, updateSongForm } = model;

export default {
  createSongFormValidation: Yup.object().shape({
    [createSongForm.formField.title.name]: Yup.string().required(
      `${createSongForm.formField.title.requiredErrorMsg}`
    ),
    [createSongForm.formField.artist.name]: Yup.string().required(
      `${createSongForm.formField.artist.requiredErrorMsg}`
    ),
  }),
  updateSongFormValidation: Yup.object().shape({
    [updateSongForm.formField.title.name]: Yup.string().required(
      `${updateSongForm.formField.title.requiredErrorMsg}`
    ),
    [updateSongForm.formField.artist.name]: Yup.string().required(
      `${updateSongForm.formField.artist.requiredErrorMsg}`
    ),
  }),
};
