import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    marginTop: '25%',
    marginLeft: '50%',
    transform: 'translate(-50%, -25%)',
  },
  mb: {
    marginBottom: '4em',
  },
}));
formM;
