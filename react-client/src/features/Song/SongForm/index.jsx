import React from 'react';
import { Grid } from '@material-ui/core';

import InputField from '../../shared/InputField';

const SongForm = props => {
  const {
    formField: { title, artist },
  } = props;

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      style={{ padding: '10px' }}
    >
      <Grid item style={{ width: 'inherit' }}>
        <InputField name={title.name} label={title.label} fullWidth />
      </Grid>
      <Grid item style={{ width: 'inherit' }}>
        <InputField name={artist.name} label={artist.label} fullWidth />
      </Grid>
    </Grid>
  );
};

export default SongForm;
