export default {
  createSongForm: {
    formId: 'songForm',
    formField: {
      title: {
        name: 'title',
        label: 'Title*',
        requiredErrorMsg: 'Title is required',
      },
      artist: {
        name: 'artist',
        label: 'Artist*',
        requiredErrorMsg: 'Artist is required',
      },
    },
  },
  updateSongForm: {
    formId: 'signUpForm',
    formField: {
      title: {
        name: 'title',
        label: 'Title*',
        requiredErrorMsg: 'Title is required',
      },
      artist: {
        name: 'artist',
        label: 'Artist*',
        requiredErrorMsg: 'Artist is required',
      },
    },
  },
};
