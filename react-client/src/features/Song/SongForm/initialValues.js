import model from './model';

const { createSongForm, updateSongForm } = model;

export default {
  initialCreateSongFormValue: {
    [createSongForm.formField.title.name]: '',
    [createSongForm.formField.artist.name]: '',
  },

  initialUpdateSongFormValue: {
    [updateSongForm.formField.title.name]: '',
    [updateSongForm.formField.artist.name]: '',
  },
};
