import { api } from '../../helpers/api';
import { setCurrentUser, removeCurrentUser } from '../../helpers/auth';

export const SIGNUP_REQUEST_PROCESS = 'SIGNUP_REQUEST_PROCESS';
export const SIGNUP_REQUEST_ERROR = 'SIGNUP_REQUEST_ERROR';
export const SIGNUP_SUCCESS = 'SIGNUP_REQUEST_SUCCESS';

export const SIGNIN_REQUEST_PROCESS = 'SIGNIN_REQUEST_PROCESS';
export const SIGNIN_REQUEST_ERROR = 'SIGNIN_REQUEST_ERROR';
export const SIGNIN_SUCCESS = 'SIGNIN_SUCCESS';
export const LOGOUT = 'LOGOUT';

export const signupRequestProcess = () => ({ type: SIGNUP_REQUEST_PROCESS });

export const signupSuccess = data => ({
  type: SIGNUP_SUCCESS,
  data,
});

export const signupRequestError = error => ({
  type: SIGNUP_REQUEST_ERROR,
  error,
});

export const signupRequest = formData => async dispatch => {
  try {
    dispatch(signupRequestProcess());

    const data = await api('post', 'auth/signup', formData);

    dispatch(signupSuccess(data));
  } catch (error) {
    dispatch(signupRequestError(error.response ? error.response.data : error));
  }
};

export const signinRequestProcess = () => ({ type: SIGNIN_REQUEST_PROCESS });

export const signinSuccess = data => ({
  type: SIGNIN_SUCCESS,
  data,
});

export const signinRequestError = error => ({
  type: SIGNIN_REQUEST_ERROR,
  error,
});

export const logout = () => ({ type: LOGOUT });

export const signinRequest = formData => async dispatch => {
  try {
    dispatch(signinRequestProcess());

    const data = await api('post', 'auth/login', formData);

    setCurrentUser(data);

    dispatch(signinSuccess(data));
  } catch (error) {
    dispatch(signinRequestError(error.response ? error.response.data : error));
  }
};

export const logoutHandler = () => dispatch => {
  removeCurrentUser();
  dispatch(logout());
};
