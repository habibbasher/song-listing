import { combineReducers } from 'redux';

import {
  SIGNUP_REQUEST_PROCESS,
  SIGNUP_REQUEST_ERROR,
  SIGNUP_SUCCESS,
  SIGNIN_REQUEST_PROCESS,
  SIGNIN_SUCCESS,
  SIGNIN_REQUEST_ERROR,
  LOGOUT,
} from './actions';

const signUpInitialState = {
  isSuccess: false,
  isLoading: false,
  isError: false,
  errorMessage: '',
};

export const signupReducer = (state = signUpInitialState, action) => {
  switch (action.type) {
    case SIGNUP_REQUEST_PROCESS:
      return { ...state, isError: false, errorMessage: '', isLoading: true };
    case SIGNUP_REQUEST_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        errorMessage: action.error.message,
      };
    case SIGNUP_SUCCESS:
      return {
        ...state,
        isSuccess: true,
        isLoading: false,
      };
    default:
      return state;
  }
};

const signInInitialState = {
  isLoading: false,
  isError: false,
  errorMessage: '',
  isAuth: false,
  user: {
    id: '',
    name: '',
    email: '',
  },
};

export const signinReducer = (state = signInInitialState, action) => {
  switch (action.type) {
    case SIGNIN_REQUEST_PROCESS:
      return { ...state, isError: false, errorMessage: '', isLoading: true };
    case SIGNIN_REQUEST_ERROR:
      return {
        ...state,
        isLoading: false,
        isError: true,
        errorMessage: action.error.message,
      };
    case SIGNIN_SUCCESS:
      return {
        ...state,
        isLoading: false,
        isAuth: true,
        user: action.data.user,
      };
    case LOGOUT:
      return { ...state, isAuth: false };
    default:
      return state;
  }
};

export const reducers = combineReducers({
  signin: signinReducer,
  signup: signupReducer,
});
