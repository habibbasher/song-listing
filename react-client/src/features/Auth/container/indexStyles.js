import { makeStyles } from '@material-ui/core/styles';

export const useStyles = makeStyles(theme => ({
  root: {
    position: 'absolute',
    marginTop: '25%',
    marginLeft: '50%',
    transform: 'translate(-50%, -25%)',
  },
  mb: {
    marginBottom: '4em',
  },
  tabs: {
    margin: '-24px',
  },
  tab: {
    'ant-tabs-tab': {
      marginLeft: '0 !important',
    },
    padding: 0,
  },
}));
