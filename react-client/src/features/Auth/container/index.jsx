import React from 'react';

import { Grid, Typography } from '@material-ui/core';
import { Tabs, Card } from 'antd';

import SignIn from '../components/SignIn';
import SignUp from '../components/SignUp';
import { useStyles } from './indexStyles';

const { TabPane } = Tabs;

const Auth = () => {
  const classes = useStyles();
  return (
    <div className={classes.root}>
      <Grid container direction="column" alignItems="center">
        <Grid item className={classes.mb}>
          <Typography variant="h3" gutterBottom>
            Song Listing
          </Typography>
        </Grid>
        <Grid item>
          <div className="card-container">
            <Card style={{ width: 350 }}>
              <Tabs type="card" className={classes.tabs}>
                <TabPane tab="SignIn" key="signin">
                  <SignIn />
                </TabPane>
                <TabPane tab="SignUp" key="signup">
                  <SignUp />
                </TabPane>
              </Tabs>
            </Card>
          </div>
        </Grid>
      </Grid>
    </div>
  );
};

export default Auth;
