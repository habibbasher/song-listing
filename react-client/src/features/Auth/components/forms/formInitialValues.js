import formModel from './formModel';

const { signInForm, signUpForm } = formModel;

export default {
  initialSignInFormValue: {
    [signInForm.formField.email.name]: '',
    [signInForm.formField.password.name]: '',
  },
  initialSignUpFormValue: {
    [signUpForm.formField.email.name]: '',
    [signUpForm.formField.password.name]: '',
    [signUpForm.formField.confirmPassword.name]: '',
  },
};
