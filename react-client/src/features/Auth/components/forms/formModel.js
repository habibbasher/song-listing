export default {
  signInForm: {
    formId: 'signInForm',
    formField: {
      email: {
        name: 'email',
        label: 'Email*',
        requiredErrorMsg: 'Email is required',
      },
      password: {
        name: 'password',
        label: 'Password*',
        requiredErrorMsg: 'Password is required',
        invalidErrorMsg: 'Password min length must be 6',
      },
    },
  },
  signUpForm: {
    formId: 'signUpForm',
    formField: {
      email: {
        name: 'email',
        label: 'Email*',
        requiredErrorMsg: 'Email is required',
      },
      password: {
        name: 'password',
        label: 'Password*',
        requiredErrorMsg: 'Password is required',
        invalidErrorMsg: 'Password min length must be 6',
      },
      confirmPassword: {
        name: 'confirmPassword',
        label: 'Confirm Password*',
        requiredErrorMsg: 'Confirm Password is required',
        invalidErrorMsg: 'Confirm Password min length must be 6',
        missMatchErrorMsg: 'Passwords must match',
      },
    },
  },
};
