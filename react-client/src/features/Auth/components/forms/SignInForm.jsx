import React from 'react';
import { Grid } from '@material-ui/core';

import InputField from '../../../shared/InputField';

const SignInForm = props => {
  const {
    formField: { email, password },
  } = props;

  return (
    <Grid
      container
      direction="column"
      alignItems="center"
      style={{ padding: '10px' }}
    >
      <Grid item style={{ width: 'inherit' }}>
        <InputField name={email.name} label={email.label} fullWidth />
      </Grid>
      <Grid item style={{ width: 'inherit' }}>
        <InputField name={password.name} label={password.label} fullWidth />
      </Grid>
    </Grid>
  );
};

export default SignInForm;
