import * as Yup from 'yup';

import formModel from './formModel';

const { signInForm, signUpForm } = formModel;

export default {
  signInFormValidation: Yup.object().shape({
    [signInForm.formField.email.name]: Yup.string().required(
      `${signInForm.formField.email.requiredErrorMsg}`
    ),
    [signInForm.formField.password.name]: Yup.string()
      .required(`${signInForm.formField.password.requiredErrorMsg}`)
      .test(
        'len',
        `${signInForm.formField.password.invalidErrorMsg}`,
        val => val && val.length >= 6
      ),
  }),
  signUpFormValidation: Yup.object().shape({
    [signUpForm.formField.email.name]: Yup.string().required(
      `${signUpForm.formField.email.requiredErrorMsg}`
    ),
    [signUpForm.formField.password.name]: Yup.string()
      .required(`${signUpForm.formField.password.requiredErrorMsg}`)
      .test(
        'len',
        `${signUpForm.formField.password.invalidErrorMsg}`,
        val => val && val.length >= 6
      ),
    [signUpForm.formField.confirmPassword.name]: Yup.string()
      .required(`${signUpForm.formField.password.requiredErrorMsg}`)
      .test(
        'len',
        `${signUpForm.formField.password.invalidErrorMsg}`,
        val => val && val.length >= 6
      )
      .oneOf(
        [Yup.ref('password'), null],
        `${signUpForm.formField.confirmPassword.missMatchErrorMsg}`
      ),
  }),
};
