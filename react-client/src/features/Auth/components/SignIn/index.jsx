import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { signinRequest } from '../../actions';

import { Button, CircularProgress } from '@material-ui/core';
import { Formik, Form } from 'formik';

import { useStyles } from './indexStyles';
import SignInForm from '../forms/SignInForm';
import formInitialValues from '../forms/formInitialValues';
import validationSchema from '../forms/validationSchema';
import formModel from '../forms/formModel';

const { formId, formField } = formModel['signInForm'];

const SignIn = props => {
  const classes = useStyles();
  const history = useHistory();

  useEffect(() => {
    props.isAuth && history.push('/');
  });

  const _handleSubmit = async (values, actions) => {
    console.log('values ', values);
    console.log('actions ', actions);
    props.signinRequest({ ...values });
    history.push('/');
  };

  return (
    <>
      <Formik
        initialValues={formInitialValues['initialSignInFormValue']}
        validationSchema={validationSchema['signInFormValidation']}
        onSubmit={_handleSubmit}
      >
        {({ isSubmitting }) => (
          <Form id={formId}>
            <SignInForm formField={formField} />

            <div className={classes.buttons}>
              <div className={classes.wrapper}>
                <Button
                  disabled={isSubmitting}
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                >
                  Sign In
                </Button>
                {isSubmitting && (
                  <CircularProgress
                    size={24}
                    className={classes.buttonProgress}
                  />
                )}
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

SignIn.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isAuth: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  signinRequest: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.auth.signin.isLoading,
  isAuth: state.auth.signin.isAuth,
  isError: state.auth.signin.isError,
  errorMessage: state.auth.signin.errorMessage,
});

export default connect(mapStateToProps, { signinRequest })(SignIn);
