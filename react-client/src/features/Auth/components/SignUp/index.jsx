import React, { useEffect } from 'react';
import { useHistory } from 'react-router-dom';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';

import { signupRequest } from '../../actions';

import { Button, CircularProgress } from '@material-ui/core';
import { Formik, Form } from 'formik';

import { useStyles } from './indexStyles';
import SignUpForm from '../forms/SignUpForm';
import formInitialValues from '../forms/formInitialValues';
import validationSchema from '../forms/validationSchema';
import formModel from '../forms/formModel';

const { formId, formField } = formModel['signUpForm'];

const SignUp = props => {
  const classes = useStyles();
  const history = useHistory();

  useEffect(() => {
    props.isAuth && history.push('/');
  });

  const _handleSubmit = async (values, actions) => {
    console.log('values ', values);
    console.log('actions ', actions);
    history.push('/');
  };

  return (
    <>
      <Formik
        initialValues={formInitialValues['initialSignUpFormValue']}
        validationSchema={validationSchema['signUpFormValidation']}
        onSubmit={_handleSubmit}
      >
        {({ isSubmitting }) => (
          <Form id={formId}>
            <SignUpForm formField={formField} />

            <div className={classes.buttons}>
              <div className={classes.wrapper}>
                <Button
                  disabled={isSubmitting}
                  type="submit"
                  variant="contained"
                  color="primary"
                  className={classes.button}
                >
                  Register
                </Button>
                {isSubmitting && (
                  <CircularProgress
                    size={24}
                    className={classes.buttonProgress}
                  />
                )}
              </div>
            </div>
          </Form>
        )}
      </Formik>
    </>
  );
};

SignUp.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  isAuth: PropTypes.bool.isRequired,
  isSuccess: PropTypes.bool.isRequired,
  isError: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
  signupRequest: PropTypes.func.isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.auth.signup.isLoading,
  isAuth: state.auth.signin.isAuth,
  isError: state.auth.signup.isError,
  isSuccess: state.auth.signup.isSuccess,
  errorMessage: state.auth.signup.errorMessage,
});

export default connect(mapStateToProps, { signupRequest })(SignUp);
