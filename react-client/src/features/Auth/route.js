import Auth from './container';

export const route = {
  path: '/auth',
  component: Auth,
  isAuth: false,
  exact: true,
};
