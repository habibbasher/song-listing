import React from 'react';

import { Grid, Divider } from '@material-ui/core';

import Banner from '../../shared/Banner';

import CardItem from '../../shared/Card';
import PaginationComponent from '../../shared/Pagination';

const Home = () => {
  return (
    <div style={{ marginTop: '50px' }}>
      <Banner heading="Home" />
      <Grid container direction="row">
        <PaginationComponent />
      </Grid>
      <Divider style={{ margin: '0 30px' }} />
      <Grid container direction="row" style={{ margin: '0 30px' }}>
        <CardItem
          songTitle="Song Title"
          artist="Artist name"
          publisher="Publisher name"
        />
        <CardItem
          songTitle="Song Title"
          artist="Artist name"
          publisher="Publisher name"
        />
        <CardItem
          songTitle="Song Title"
          artist="Artist name"
          publisher="Publisher name"
        />
        <CardItem
          songTitle="Song Title"
          artist="Artist name"
          publisher="Publisher name"
        />
        <CardItem
          songTitle="Song Title"
          artist="Artist name"
          publisher="Publisher name"
        />
        <CardItem
          songTitle="Song Title"
          artist="Artist name"
          publisher="Publisher name"
        />
        <CardItem
          songTitle="Song Title"
          artist="Artist name"
          publisher="Publisher name"
        />
      </Grid>
    </div>
  );
};

export default Home;
