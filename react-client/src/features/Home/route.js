import Home from './container';

export const route = {
  path: '/',
  component: Home,
  isAuth: false,
  exact: true,
};
