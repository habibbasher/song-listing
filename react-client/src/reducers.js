import { combineReducers } from 'redux';

import { authReducers } from './features/Auth';
import { songReducer } from './features/Song';
// import { locationReducer } from "./feature/API/locationReducer";
// import { profileReducer } from "./feature/Cabinet/Profile";

export const reducers = combineReducers({
  auth: authReducers,
  song: songReducer,
  // cabinet: cabinetReducers,
  // location: locationReducer,
  // profile: profileReducer,
});
