import { homeRoute } from './features/Home';
import { authRoute } from './features/Auth';
import { songRoutes } from './features/Song';
import { notFoundRoute } from './features/NotFound';

export const routes = [homeRoute, authRoute, ...songRoutes, notFoundRoute];
