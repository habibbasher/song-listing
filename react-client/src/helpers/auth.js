import decode from 'jwt-decode';

import strings from './strings';

export const getCurrentUser = () => {
  const data = localStorage.getItem(strings.CURRENT_USER);
  return data ? JSON.parse(data) : null;
};

export const setCurrentUser = data => {
  localStorage.setItem(strings.CURRENT_USER, JSON.stringify(data));
};

export const removeCurrentUser = () => {
  localStorage.removeItem(strings.CURRENT_USER);
};

export const checkTokenExpire = token => {
  try {
    const { exp } = decode(token);
    const currentTime = new Date().getTime() / 1000;

    if (currentTime > exp) {
      return false;
    }

    return true;
  } catch (error) {
    return false;
  }
};
