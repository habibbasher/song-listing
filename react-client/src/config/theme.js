import { createMuiTheme, responsiveFontSizes } from '@material-ui/core/styles';
import { blue, orange, red } from '@material-ui/core/colors';

let theme = createMuiTheme({
  palette: {
    primary: {
      main: blue.A400,
    },
    secondary: {
      main: orange.A400,
    },
    error: {
      main: red.A400,
    },
    background: {
      default: '#fff',
    },
  },
});

theme = responsiveFontSizes(theme);

export default theme;
