import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { Provider } from 'react-redux';

import { store } from './store';

import { routes } from './routes';
import AuthRoute from './features/shared/AuthRoute';
import Layout from './features/shared/Layout';
// console.log('routes', routes);
import { signinSuccess, logout } from './features/Auth/actions';

import { getCurrentUser } from './helpers/auth';
const currentUser = getCurrentUser();
if (currentUser) {
  store.dispatch(signinSuccess(currentUser));
}

const App = () => {
  return (
    <div style={{ overflow: 'hidden' }}>
      <Provider store={store}>
        <Layout>
          <Switch>
            {routes.map((item, i) =>
              item.isAuth ? (
                <AuthRoute
                  path={item.path}
                  component={item.component}
                  key={i}
                  exact={item.exact}
                />
              ) : (
                <Route
                  path={item.path}
                  component={item.component}
                  key={i}
                  exact={item.exact}
                />
              )
            )}
          </Switch>
        </Layout>
      </Provider>
    </div>
  );
};

export default App;
