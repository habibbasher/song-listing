import SongModel from '../models/Song';

const getSongs = async (queryParams, publisherId = null) => {
  const limit =
    queryParams.limit && parseInt(queryParams.limit)
      ? parseInt(queryParams.limit)
      : 10;
  const skip =
    queryParams.skip && parseInt(queryParams.skip)
      ? parseInt(queryParams.skip)
      : 0;

  let songs;
  if (publisherId) {
    songs = await SongModel.find({
      publishedBy: publisherId,
    })
      .limit(limit)
      .skip(skip);

    return songs;
  }

  songs = await SongModel.find({}).limit(limit).skip(skip);

  return songs;
};

const getSong = async (id, publisherId) => {
  const song = await SongModel.findOne({ _id: id, publishedBy: publisherId });
  if (song) {
    return song;
  }

  return {};
};

export default {
  getSongs,
  getSong,
};
