import jwt from 'jsonwebtoken';
import config from '../config/token';
import AppError from '../exceptions/AppError';

const sign = (payload, secretToken, options) => {
  try {
    const token = jwt.sign(payload, secretToken, options);
    return token;
  } catch (err) {
    throw new AppError(err.message);
  }
};

const createAccessToken = user => {
  try {
    const payload = {
      id: user._id,
    };

    const options = {
      algorithm: 'HS512',
      subject: user._id.toString(),
      expiresIn: config.expireAccess,
    };

    const token = sign(payload, config.secretAccess, options);

    return token;
  } catch (err) {
    throw new AppError(err.message);
  }
};

const verifyAccessToken = token => {
  try {
    const data = jwt.verify(token, config.secretAccess);

    return data;
  } catch (err) {
    return false;
  }
};

export default {
  sign,
  createAccessToken,
  verifyAccessToken,
};
