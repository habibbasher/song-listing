import util from 'util';
import multer from 'multer';
import { v4 as uuidv4 } from 'uuid';

const maxSize = 2 * 1024 * 1024;

let storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, __basedir + '/uploads/');
  },
  filename: (req, file, cb) => {
    console.log('file', file);
    file.originalname = `${uuidv4()}-${file.originalname}`;
    console.log(file.originalname);
    cb(null, file.originalname);
  },
});

let uploadFile = util.promisify(
  multer({
    storage: storage,
    limits: { fileSize: maxSize },
  }).single('file')
);

export default { uploadFile };
