import { Router } from 'express';
import AuthController from '../controllers/AuthController';
import Validate from '../middleware/Validate';
import Authorize from '../middleware/Authorize';
import { authSchemas } from '../schemas';

const router = Router();

router.post(
  '/auth/login',
  Validate.prepare(authSchemas.signIn),
  AuthController.signIn
);
router.post(
  '/auth/signup',
  Validate.prepare(authSchemas.signUp),
  AuthController.signUp
);

router.post('/auth/logout', Authorize.check, AuthController.logout);

export default router;
