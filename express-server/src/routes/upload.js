import { Router } from 'express';
import ImageUploadController from '../controllers/ImageUploadController';

const router = Router();

router.post('/file/upload', ImageUploadController.uploadFile);
router.get('/files/:name', ImageUploadController.downloadFile);

export default router;
