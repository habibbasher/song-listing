import { Router } from 'express';

import SongController from '../controllers/SongController';
import Validate from '../middleware/Validate';
import { songSchema } from '../schemas';
import Authorize from '../middleware/Authorize';

const router = Router();

router.post(
  '/songs',
  Authorize.check,
  Validate.prepare(songSchema),
  SongController.create
);

router.get('/songs', Authorize.check, SongController.getSongs);
router.get('/mylist', Authorize.check, SongController.getUserSongs);
router.get('/song/:id', Authorize.check, SongController.getSong);

export default router;
