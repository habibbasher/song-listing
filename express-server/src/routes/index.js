import users from './users';
import auth from './auth';
import upload from './upload';
import songs from './song';

export default [users, auth, upload, songs];
