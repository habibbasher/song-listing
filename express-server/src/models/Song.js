import mongoose, { Schema } from 'mongoose';
import uniqueValidator from 'mongoose-unique-validator';

const SongSchema = new Schema(
  {
    title: {
      type: String,
      required: true,
      trim: true,
    },
    artist: {
      type: String,
      required: true,
      trim: true,
    },
    publishedBy: {
      type: String,
    },
    coverImgLink: {
      type: String,
    },
  },
  {
    timestamps: true,
  }
);

SongSchema.plugin(uniqueValidator);

export default mongoose.model('Song', SongSchema);
