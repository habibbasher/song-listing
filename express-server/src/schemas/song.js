const song = {
  type: 'object',
  required: ['title', 'artist'],
  properties: {
    title: {
      type: 'string',
      errorMessage: {
        type: "Field 'email' should be a string",
      },
    },
    artist: {
      type: 'string',
      errorMessage: {
        type: "Field 'artist' should be a string",
      },
    },
    publishedBy: {
      type: 'string',
      errorMessage: {
        type: "Field 'publishedBy' should be a string",
      },
    },
    coverImgLink: {
      type: 'string',
      errorMessage: {
        type: "Field 'coverImgLink' should be a string",
      },
    },
  },
};

export default song;
