const signIn = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
      errorMessage: {
        format: "Field 'email' incorrect",
        type: "Field 'email' should be a string",
      },
    },
    password: {
      type: 'string',
      errorMessage: {
        type: "Field 'password' should be a string",
      },
    },
  },
};

const signUp = {
  type: 'object',
  required: ['email', 'password'],
  properties: {
    email: {
      type: 'string',
      format: 'email',
      errorMessage: {
        format: "Field 'email' incorrect",
        type: "Field 'email' should be a string",
      },
    },
    password: {
      type: 'string',
      minLength: 6,
      maxLength: 30,
      errorMessage: {
        type: "Field 'password' should be a string",
      },
    },
  },
};

export default {
  signIn,
  signUp,
};
