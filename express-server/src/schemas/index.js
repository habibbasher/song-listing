import auth from './auth';
import song from './song';

export const authSchemas = auth;
export const songSchema = song;
