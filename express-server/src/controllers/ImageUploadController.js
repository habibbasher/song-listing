import config from '../config/app';
import TryCatchErrorDecorator from '../decorators/TryCatchErrorDecorator';
import ClientError from '../exceptions/ClientError';

import UploadService from '../services/UploadService';

class ImageUploadController {
  @TryCatchErrorDecorator
  static async uploadFile(req, res) {
    console.log('ImageUploadController -> uploadFile -> req', req.body);

    const baseUrl = `${config.host}:${config.port}/uploads/`;

    if (req.file == undefined) {
      throw new ClientError('Please upload a file!', 400);
    }

    await UploadService.uploadFile(req, res);

    res.status(200).send({
      name: req.file.originalname,
      imageUrl: baseUrl + req.file.originalname,
      message: 'Uploaded the file successfully: ' + req.file.originalname,
    });
  }

  @TryCatchErrorDecorator
  static async downloadFile(req, res) {
    const fileName = req.params.name;
    const directoryPath = __basedir + '/uploads/';

    res.download(directoryPath + fileName, fileName, err => {
      if (err) {
        res.status(500).send({
          message: 'Could not download the file. ' + err,
        });
      }
    });
  }
}

export default ImageUploadController;
