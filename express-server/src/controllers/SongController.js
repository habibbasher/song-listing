import SongModel from '../models/Song';
import PasswordService from '../services/PasswordService';
import ClientError from '../exceptions/ClientError';
import TryCatchErrorDecorator from '../decorators/TryCatchErrorDecorator';
import TokenService from '../services/TokenService';
import AppError from '../exceptions/AppError';

import SongService from '../services/SongService';

class SongController {
  @TryCatchErrorDecorator
  static async create(req, res) {
    const song = new SongModel({
      title: req.body.title,
      artist: req.body.artist,
      publishedBy: req.userId,
      coverImgLink: req.body.coverImgLink,
    });

    await song.save();

    res.status(201).json({ status: 'success' });
  }

  @TryCatchErrorDecorator
  static async getSongs(req, res) {
    // const publisherId = req.userId;
    const queryParams = {
      limit: req.query.limit,
      skip: req.query.skip,
    };

    const songs = await SongService.getSongs(queryParams);
    res.status(201).json(songs);
  }

  @TryCatchErrorDecorator
  static async getUserSongs(req, res) {
    const publisherId = req.userId;
    const queryParams = {
      limit: req.query.limit,
      skip: req.query.skip,
    };

    const songs = await SongService.getSongs(queryParams, publisherId);
    res.status(201).json(songs);
  }

  @TryCatchErrorDecorator
  static async getSong(req, res) {
    const publisherId = req.userId;
    const id = req.params.id;

    const song = await SongService.getSong(id, publisherId);
    res.status(201).json(song);
  }
}

export default SongController;
