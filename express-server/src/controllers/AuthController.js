import UserModel from '../models/User';
import PasswordService from '../services/PasswordService';
import ClientError from '../exceptions/ClientError';
import TryCatchErrorDecorator from '../decorators/TryCatchErrorDecorator';
import TokenService from '../services/TokenService';
import AppError from '../exceptions/AppError';

class AuthController {
  @TryCatchErrorDecorator
  static async signIn(req, res) {
    const user = await UserModel.findOne({ email: req.body.email });
    if (!user) {
      throw new ClientError('User not found', 404);
    }

    const checkPassword = await PasswordService.checkPassword(
      req.body.password,
      user.password
    );

    if (!checkPassword) {
      throw new ClientError('Incorrect email or password', 401);
    }

    const accessToken = TokenService.createAccessToken(user);

    res.json({
      accessToken,
      user: {
        id: user.id,
        email: user.email,
      },
    });
  }

  @TryCatchErrorDecorator
  static async signUp(req, res) {
    const isAlreadyUser = await UserModel.findOne({ email: req.body.email });
    if (isAlreadyUser) {
      throw new ClientError('This email is already registered', 409);
    }

    const user = new UserModel({
      email: req.body.email,
      password: await PasswordService.hashPassword(req.body.password),
    });

    await user.save();

    res.json({ status: 'success', statusCode: 201 });
  }

  @TryCatchErrorDecorator
  static async logout(req, res, next) {
    const user = await UserModel.findOne({ _id: req.userId });
    if (!user) {
      throw new AppError('UserId not found in request', 401);
    }

    user.refreshTokens = [];
    await user.save();

    res.json({ status: 'success', statusCode: 200 });
  }
}

export default AuthController;
